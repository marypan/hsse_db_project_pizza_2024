-- Список всех клиентов пиццерии
create view clients_names as
select name, surname
from pizzeria.clients;

-- Обновляемое представление, через которое можно изменить количество ингредиента
create view amount_of_ingredients as
select name, amount
from pizzeria.ingredients;

-- Таблица состава пицц для клиентов
create view pizza_with_ingredients as
select type, name
from pizzeria.menu
         join pizzeria.ingredients_for_pizza as ingr_for_pizza on menu.id = ingr_for_pizza.pizza_id
         join pizzeria.ingredients as ingr on ingr.id = ingr_for_pizza.ingredient_id;

-- Таблица с заказами: заказчик и курьер, который к нему прикреплен
create view clients_and_couriers as
select clients.name          as client_name,
       clients.surname       as client_surname,
       clients.mobile_phone  as client_phone,
       couriers.name         as courier_name,
       couriers.surname      as courier_surname,
       couriers.mobile_phone as courier_phone
from pizzeria.orders
         join pizzeria.clients as clients on orders.customer_id = clients.id
         join pizzeria.couriers as couriers on orders.courier_id = couriers.id;