insert into pizzeria.menu (type, size, price)
values ('Сырная', 30, 400),
       ('Пепперони', 35, 550),
       ('Двойной цыплёнок', 25, 300),
       ('Ветчина и сыр', 30, 400),
       ('Гавайская', 25, 300),
       ('Маргарита', 35, 550),
       ('Овощи и грибы', 30, 400),
       ('Морская', 30, 400),
       ('Домашняя', 30, 400),
       ('Карбонара', 30, 400),
       ('Бургер пицца', 25, 350),
       ('Ветчина и грибы', 30, 450),
       ('Колбаски барбекю', 25, 400),
       ('Жульен', 30, 400),
       ('Студенческая', 35, 470);


insert into pizzeria.ingredients (name, amount)
values ('Мука', 1000),
       ('Колбаски охотничьи', 30),
       ('Сосиски', 30),
       ('Ветчина', 50),
       ('Томаты', 300),
       ('Лук', 60),
       ('Огурцы маринованные', 300),
       ('Курица', 200),
       ('Соус томатный', 100),
       ('Соус сырный', 100),
       ('Соус гавайский', 100),
       ('Маслины', 70),
       ('Оливки', 50),
       ('Сыр моцарелла', 500),
       ('Сыр пармезан', 500),
       ('Сыр чеддер', 400),
       ('Сыр кордон-блю', 300),
       ('Кетчуп', 70),
       ('Майонез', 100),
       ('Грибы', 50),
       ('Ананасы', 50),
       ('Колбаса пепперони', 100),
       ('Соус альфредо', 100),
       ('Креветки', 50);


insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Сырная'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Сырная'),
        (select id from pizzeria.ingredients where name = 'Сыр пармезан')),
       ((select id from pizzeria.menu where type = 'Сырная'),
        (select id from pizzeria.ingredients where name = 'Сыр чеддер')),
       ((select id from pizzeria.menu where type = 'Сырная'),
        (select id from pizzeria.ingredients where name = 'Сыр кордон-блю'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Пепперони'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Пепперони'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Пепперони'),
        (select id from pizzeria.ingredients where name = 'Колбаса пепперони')),
       ((select id from pizzeria.menu where type = 'Пепперони'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Двойной цыплёнок'),
        (select id from pizzeria.ingredients where name = 'Курица')),
       ((select id from pizzeria.menu where type = 'Двойной цыплёнок'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Двойной цыплёнок'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Ветчина и сыр'),
        (select id from pizzeria.ingredients where name = 'Ветчина')),
       ((select id from pizzeria.menu where type = 'Ветчина и сыр'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Ветчина и сыр'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Ветчина')),
       ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Огурцы маринованные')),
       ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Лук')),
       ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Бургер пицца'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Гавайская'),
        (select id from pizzeria.ingredients where name = 'Курица')),
       ((select id from pizzeria.menu where type = 'Гавайская'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Гавайская'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо')),
       ((select id from pizzeria.menu where type = 'Гавайская'),
        (select id from pizzeria.ingredients where name = 'Ананасы'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Ветчина и грибы'),
        (select id from pizzeria.ingredients where name = 'Ветчина')),
       ((select id from pizzeria.menu where type = 'Ветчина и грибы'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Ветчина и грибы'),
        (select id from pizzeria.ingredients where name = 'Соус томатный')),
       ((select id from pizzeria.menu where type = 'Ветчина и грибы'),
        (select id from pizzeria.ingredients where name = 'Грибы'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Маргарита'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Маргарита'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Маргарита'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Колбаски барбекю'),
        (select id from pizzeria.ingredients where name = 'Колбаски охотничьи')),
       ((select id from pizzeria.menu where type = 'Колбаски барбекю'),
        (select id from pizzeria.ingredients where name = 'Лук')),
       ((select id from pizzeria.menu where type = 'Колбаски барбекю'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Колбаски барбекю'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Колбаски барбекю'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Лук')),
       ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Огурцы маринованные')),
       ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Соус томатный')),
       ((select id from pizzeria.menu where type = 'Овощи и грибы'),
        (select id from pizzeria.ingredients where name = 'Грибы'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Морская'),
        (select id from pizzeria.ingredients where name = 'Ананасы')),
       ((select id from pizzeria.menu where type = 'Морская'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Морская'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо')),
       ((select id from pizzeria.menu where type = 'Морская'),
        (select id from pizzeria.ingredients where name = 'Креветки'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Курица')),
       ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Грибы')),
       ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Сыр чеддер')),
       ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Сыр пармезан')),
       ((select id from pizzeria.menu where type = 'Жульен'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Студенческая'),
        (select id from pizzeria.ingredients where name = 'Сосиски')),
       ((select id from pizzeria.menu where type = 'Студенческая'),
        (select id from pizzeria.ingredients where name = 'Сыр пармезан')),
       ((select id from pizzeria.menu where type = 'Студенческая'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Студенческая'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Студенческая'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));


insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Домашняя'),
        (select id from pizzeria.ingredients where name = 'Ветчина')),
       ((select id from pizzeria.menu where type = 'Домашняя'),
        (select id from pizzeria.ingredients where name = 'Огурцы маринованные')),
       ((select id from pizzeria.menu where type = 'Домашняя'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Домашняя'),
        (select id from pizzeria.ingredients where name = 'Сыр пармезан')),
       ((select id from pizzeria.menu where type = 'Домашняя'),
        (select id from pizzeria.ingredients where name = 'Соус томатный'));

insert into pizzeria.ingredients_for_pizza (pizza_id, ingredient_id)
values ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Сыр чеддер')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Сыр пармезан')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Сыр моцарелла')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Ветчина')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Томаты')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Лук')),
       ((select id from pizzeria.menu where type = 'Карбонара'),
        (select id from pizzeria.ingredients where name = 'Соус альфредо'));


insert into pizzeria.clients (name, surname, mobile_phone, address)
values ('Владислав', 'Новиков', '8-911-234-56-78', 'Улица Ленина, дом 10, подъезд 2, квартира 5'),
       ('Артем', 'Орехов', '8-922-345-67-89', 'Проезд Садовый, дом 7, подъезд 3, квартира 15'),
       ('Татьяна', 'Игнатьева', '8-933-456-78-90', 'Проспект Мира, дом 25, подъезд 1, квартира 8'),
       ('Алина', 'Казакова', '8-944-567-89-01', 'Улица Тверская, дом 14, подъезд 4, квартира 21'),
       ('Ксения', 'Потапова', '8-955-678-90-12', 'Переулок Зеленый, дом 3, подъезд 2, квартира 12'),
       ('Тимофей', 'Шилов', '8-966-789-01-23', 'Проезд Ленинградский, дом 5, подъезд 3, квартира 7'),
       ('Владислав', 'Самсонов', '8-977-890-12-34', 'Улица Арбат, дом 30, подъезд 1, квартира 3'),
       ('Петр', 'Афанасьев', '8-988-901-23-45', 'Проспект Вернадского, дом 18, подъезд 2, квартира 9'),
       ('Ольга', 'Беляева', '8-999-012-34-56', 'Улица Красная Пресня, дом 12, подъезд 4, квартира 17'),
       ('Максим', 'Колесников', '8-910-123-45-67', 'Переулок Большой, дом 8, подъезд 1, квартира 6'),
       ('Василиса', 'Маслова', '8-921-234-56-78', 'Улица Новый Арбат, дом 22, подъезд 3, квартира 14'),
       ('Святослав', 'Давыдов', '8-932-345-67-89', 'Проспект Кутузовский, дом 35, подъезд 2, квартира 10'),
       ('Георгий', 'Беляков', '8-943-456-78-90', 'Улица Смоленская, дом 16, подъезд 4, квартира 23'),
       ('Ксения', 'Борисова', '8-954-567-89-01', 'Переулок Малый, дом 4, подъезд 1, квартира 11'),
       ('Елена', 'Емельянова', '8-965-678-90-12', 'Проезд Чистопрудный, дом 9, подъезд 3, квартира 18'),
       ('Глеб', 'Карпов', '8-976-789-01-23', 'Улица Волгоградский проспект, дом 27, подъезд 2, квартира 13'),
       ('Евгений', 'Архипов', '8-987-890-12-34', 'Переулок Садовый, дом 6, подъезд 1, квартира 4'),
       ('Анна', 'Павлова', '8-998-901-23-45', 'Проезд Мосфильмовский, дом 11, подъезд 3, квартира 19'),
       ('Андрей', 'Щербаков', '8-909-012-34-56', 'Улица Красносельская, дом 20, подъезд 2, квартира 16'),
       ('Александра', 'Потапова', '8-920-123-45-67', 'Проспект Ленинский, дом 40, подъезд 4, квартира 22');


insert into pizzeria.couriers (name, surname, mobile_phone, transport)
values ('Марк', 'Богданов', '8-123-456-7890', 'Велосипед'),
       ('Арсений', 'Ермаков', '8-987-654-3210', 'Самокат'),
       ('Егор', 'Гусев', '8-876-543-2109', 'Велоспипед'),
       ('Роман', 'Денисов', '8-234-567-8901', 'Машина'),
       ('Остап', 'Орехов', '8-345-678-9012', 'Пешком'),
       ('Святослав', 'Васильев', '8-456-789-0123', 'Самокат'),
       ('Михаил', 'Белозеров', '8-567-890-1234', 'Машина'),
       ('Захар', 'Шашков', '8-678-901-2345', 'Машина'),
       ('Григорий', 'Шубин', '8-789-012-3456', 'Пешком'),
       ('Назар', 'Крылов', '8-890-123-45677', 'Велосипед');

insert into pizzeria.orders (customer_id, courier_id, cost, comment)
values (1, 7, 1400, 'Добавить салфетки'),
       (12, 7, 350, 'Убрать лук'),
       (9, 5, 400, 'Оставить заказ у двери'),
       (20, 10, 400, 'Убрать сыр с плесенью');

insert into pizzeria.orders (customer_id, courier_id, cost)
values (6, 2, 950),
       (18, 3, 800),
       (14, 4, 4200),
       (10, 8, 800),
       (15, 1, 800),
       (7, 6, 900);

insert into pizzeria.composition_of_orders(order_id, pizza_id, amount)
values (1, 2, 2),
       (1, 3, 1),
       (2, 11, 1),
       (3, 8, 1),
       (4, 1, 1),
       (5, 6, 1),
       (5, 7, 1),
       (6, 4, 2),
       (7, 6, 2),
       (7, 2, 2),
       (7, 4, 3),
       (7, 1, 2),
       (8, 10, 1),
       (8, 14, 1),
       (9, 9, 1),
       (9, 15, 1),
       (10, 5, 3);