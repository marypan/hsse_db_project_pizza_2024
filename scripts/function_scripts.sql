-- Функция, которая считает прибыль за все заказы
create or replace function income() returns integer as
$$
select sum(cost)
from pizzeria.orders
$$
    language sql
    immutable;

-- Функция, которая выводит все пиццы заданного размера
create or replace function get_pizza_of_size(s integer)
    returns table
            (
                type varchar
            )
as
$$
select type
from pizzeria.menu
where size = s
$$
    language sql
    immutable;

-- Функция, которая выводит случайную пиццу - пиццу дня
create or replace function pizza_of_the_day() returns varchar as
$$
select type
from pizzeria.menu
where id = (select ceiling(1 + random() * (15 - 1)))
$$
    language sql
    volatile;


-- Триггер, который при удалении ингредиента сначала удаляет все строки,
-- в которых он присутствовал в таблице ingredients_for_pizza
create or replace function delete_ingredients_dependencies() returns trigger as
$$
begin
    delete from pizzeria.ingredients_for_pizza where ingredient_id = old.id;
    return new;
end
$$ language plpgsql;

create or replace trigger delete_ingredient
    before delete on pizzeria.ingredients
    for each row
execute procedure delete_ingredients_dependencies();