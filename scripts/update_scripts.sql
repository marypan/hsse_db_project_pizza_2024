--Изменить транспорт курьера Остап Орехов
update pizzeria.couriers
set transport = 'Самокат'
where name = 'Остап' and surname = 'Орехов';

--Добавить комментарий к заказу пользователя Максим Колесников
update pizzeria.orders
set comment = 'Не класть салфетки'
where customer_id = (select id from pizzeria.clients where name = 'Максим' and surname = 'Колесников');

--Убрать одну пиццу Пепперони из заказа с номером 1
update pizzeria.composition_of_orders
set amount = amount - 1
where order_id = 1 and pizza_id = (select id from pizzeria.menu where type = 'Пепперони');

--Изменить стоимость заказа после удаления пиццы
update pizzeria.orders
set cost = cost - (select price from pizzeria.menu where type = 'Пепперони')
where id = 1