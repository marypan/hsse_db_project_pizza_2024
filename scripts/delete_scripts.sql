--Евгений Архипов удалил свой аккаунт в приложении, удалить его из базы данных
delete from pizzeria.clients
where name = 'Евгений' and surname = 'Архипов';

--Убрать Сыр чеддер из состава пиццы Карбонара
delete from pizzeria.ingredients_for_pizza
where  pizza_id = (select id from pizzeria.menu where type = 'Карбонара') and
       ingredient_id = (select id from pizzeria.ingredients where name = 'Сыр Чеддер')