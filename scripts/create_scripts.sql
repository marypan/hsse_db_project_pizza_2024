--Создание таблиц

-- Создание схемы базы данных пиццерии
create schema pizzeria

-- Таблица-меню с разными видами пицц
create table pizzeria.menu (
    id bigserial primary key,
    type varchar not null unique, --вид пиццы
    size integer check (size = 25 or size = 30 or size = 35) not null,
    price integer check(price >= 0) not null
)

-- Таблица ингредиентов, необходимых для приготовления пиццы
create table pizzeria.ingredients (
    id bigserial primary key,
    name varchar not null unique,
    amount integer check (amount >= 0) not null
)

-- Таблица связи вида пиццы и необходимых для нее ингредиентов
create table pizzeria.ingredients_for_pizza (
    ingredient_id bigserial,
    pizza_id bigserial,
    constraint FK_ingredient foreign key (ingredient_id) references pizzeria.ingredients(id) on delete cascade,
    constraint FK_pizza foreign key (pizza_id) references pizzeria.menu(id) on delete cascade
)

-- Таблица клиентов пиццерии
create table pizzeria.clients (
    id bigserial primary key,
    name varchar not null,
    surname varchar not null,
    mobile_phone varchar not null unique,
    address varchar not null
)

-- Таблица курьеров
create table pizzeria.couriers (
    id bigserial primary key,
    name varchar not null,
    surname varchar not null,
    mobile_phone varchar not null unique,
    transport varchar not null
)

-- Таблица заказов
create table pizzeria.orders (
    id bigserial primary key,
    customer_id bigserial,
    courier_id bigserial,
    cost integer check (cost >= 0) not null,
    comment text,
    constraint FK_customer foreign key (customer_id) references pizzeria.clients(id) on delete restrict,
    constraint FK_courier foreign key (courier_id) references pizzeria.couriers(id) on delete restrict
)

-- Таблица составов заказов
create table pizzeria.composition_of_orders (
    order_id bigserial,
    pizza_id bigserial,
    amount   integer check (amount >= 0) not null,
    constraint FK_order foreign key (order_id) references pizzeria.orders(id) on delete restrict,
    constraint FK_pizza foreign key (pizza_id) references pizzeria.menu(id) on delete restrict
)