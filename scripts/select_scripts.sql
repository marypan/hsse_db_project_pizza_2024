-- Выбрать все пиццы размера 35 см
select *
from pizzeria.menu
where size = 35;

-- Посчитать общую прибыль за все заказы
select sum(cost) as income
from pizzeria.orders;

-- Выбрать всех курьеров с транспортом велосипед
select *
from pizzeria.couriers
where transport = 'Велосипед';

-- Выбрать все пиццы, в которых есть томаты
select type
from pizzeria.menu
join pizzeria.ingredients_for_pizza as ingr_for_pizza on menu.id = ingr_for_pizza.pizza_id
join pizzeria.ingredients as ingr on ingr.id = ingr_for_pizza.ingredient_id
where ingr.name = 'Томаты';

-- Проверить, корректно ли посчитана стоимость заказа с id = 7, исходя из информации в таблице composition_of_orders
with help_table as (
select pizza_id, amount
from pizzeria.composition_of_orders
where order_id = 7
)
select sum((select price from pizzeria.menu where id = help_table.pizza_id) * amount) = cost as correct_sum
from help_table, pizzeria.orders
where id = 7
group by cost;


-- Посчитать, сколько максимум пицц Пепперони можно приготовить из имеющегося количества ингредиентов в кг,
-- если задано, сколько в среднем в граммах уходит ингредиентов на одну пиццу
with help_table as (
with need_ingreedient as (
select ingredient_id
from pizzeria.ingredients_for_pizza
where pizza_id = (select id from pizzeria.menu where type = 'Пепперони')
), need_amount as (
    select id, amount, case  when name = 'Сыр моцарелла' then 250
    when name = 'Томаты' then 170
    when name = 'Колбаса пепперони' then 200
    when name = 'Соус томатный' then 150
    end as need_gramm_amount
    from pizzeria.ingredients
    )
select ingredient_id, need_gramm_amount
from need_ingreedient, need_amount
where id = ingredient_id
)
select min(round((amount * 1000) / need_gramm_amount, 0)) as max_num_of_pizza
from help_table
join pizzeria.ingredients on ingredient_id = ingredients.id;



-- Оконки

-- Посчитать для каждого курьера общую стоимость заказов, которые они доставляли
select distinct courier_id, sum(cost) over (partition by courier_id) as sum_cost
from pizzeria.orders
order by courier_id;


-- Выбрать имена заказчиков, которые встречаются больше одного раза (чтобы сделать скидку в именины)
with help_table as (select distinct name, count(name) over (partition by name) as cnts
                    from pizzeria.clients)
select name, cnts
from help_table
where cnts = (select max(cnts) from help_table);


-- Выбрать людей, которым доставлял заказ один и тот же курьер
with help_table as (
select customer_id, courier_id, count(customer_id) over (partition by courier_id) as cnt
from pizzeria.orders
)
select (select name from pizzeria.clients where id = help_table.customer_id),
       (select surname from pizzeria.clients where id = help_table.customer_id), courier_id
from help_table
where cnt >= 2;


-- Выбрать пиццы, у которых в составе >= 5 ингредиента
with help_table as (
select pizza_id, count(ingredient_id) over (partition by pizza_id) as count_of_ingr
from pizzeria.ingredients_for_pizza
)
select distinct (select type from pizzeria.menu where id = help_table.pizza_id), count_of_ingr
from help_table
where count_of_ingr >= 5;


-- Вывести топ 3 самых больших заказов по количеству пицц
select distinct order_id, sum(amount) over w as num_of_pizza_in_order
from pizzeria.composition_of_orders
window w as (
partition by order_id
order by order_id
)
order by num_of_pizza_in_order desc
limit 3;